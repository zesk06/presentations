.PHONY: dist clean
.DEFAULT_GOAL := dist

clean:
	@echo "Cleaning dist/"
	@rm -fr dist

dist: clean
	@mkdir -p dist
	@git archive --format=tar.gz -o  dist/devops.tar.gz --prefix=devops/  HEAD
	@echo "generated archive in dist/devops.tar.gz"

serve:
	# cd public && python -m http.server
	yarn serve
