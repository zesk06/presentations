# presentation VIM

Speaker: Nicolas Rouviere (nicolas.rouviere@airbus.com)

- Retrouvez les slides publiés [ICI](https://https://zesk06.gitlab.io/presentations)
- Téléchargez les slides en TAR.GZ [ICI](https://https://gitlab.com/zesk06/presentations/-/jobs)

## DEV

```bash
# install
yarn install

# serve
yarn serve

```

### ajout de dependance JS

Par exemple, raphael, Ajouter dans le ``package.json`` la ligne:

```json
    "@bower_components/raphael": "https://github.com/DmitryBaranovskiy/raphael/",
```

et ensuite taper ``yarn``
